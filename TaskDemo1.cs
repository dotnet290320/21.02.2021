using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LearnTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task t1 = new Task(() =>
            {
                Console.WriteLine($"{Task.CurrentId} hello from new Task ");
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} hello from new Task ");
                Thread.Sleep(100);
                Console.WriteLine("hello from new Task ended...");
            }
                // not from thread pool, more time to initiate...
                //, TaskCreationOptions.LongRunning
            );
            //t1.RunSynchronously(); // Start + wait
            t1.Start();
            //t1.Wait();

            Task t2 = Task.Run(() =>
            {
                Console.WriteLine($"{Task.CurrentId} hello from Task.Run ");
                Thread.Sleep(150);
                Console.WriteLine("hello from new Task.Run ended...");
            });
            //t2.RunSynchronously();
            //t2.Wait();
            //t2.Start();

            //Task.WaitAny(new[] { t1, t2, Task.Run(() => { })});
            //Task.WaitAny(new[] { t1, t2, Task.Run(() => { }) });

            Task.WaitAll(new[] { t1, t2 });

            // 1
            // create 10 tasks
            // in each task sleep a random amount of mili... 500- 2500
            // wait until one task completes (WaitAny)
            // announce the name of the winner!
            // *Etgar: solve without isng WaitAny

            List<Task> tasks_10 = new List<Task>();
            int max_tasks = 10;
            Random r = new Random();
            AutoResetEvent arm = new AutoResetEvent(true);
            for (int i = 0; i < max_tasks; i++)
            {
                int number = i + 1;
               Task t = Task.Run(() =>
               {
                   int sleep_time = r.Next(2000) + 501;
                   Console.WriteLine($"Firing task {number} will sleep {sleep_time}");
                   Thread.Sleep(sleep_time);
                   arm.WaitOne();
                   Console.WriteLine($"Winner is {number} slept {sleep_time}");
               });
                tasks_10.Add(t);
            }
            // way 1
            //Task.WaitAny(tasks_10.ToArray());


            // way 2 (without WaitAny)
            int finished_any = 0;
            while (finished_any == 0)
            {
                finished_any = 0;
                foreach (var t in tasks_10)
                {
                    finished_any += t.Status == TaskStatus.RanToCompletion ? 1 : 0;
                }
                Thread.Sleep(10);
            }

            // 2
            // create a random amount of tasks 1-10
            // in each task print the task ID
            // in each task also print the numbers from 1 to 10 with delay of 100 mili
            // wait for all tasks to finish (WaitAll)
            // * Etgar: wait for all tasks to finish without wait all
            int max_rnd = r.Next(10) + 1;
            List<Task> tasks_rnd = new List<Task>();
            for (int i = 0; i < max_rnd; i++)
            {
                int number = i + 1;
                Task t = Task.Run(() =>
                {
                    for (int j = 1; j <= 10; j++)
                    {
                        Console.WriteLine($"Task #{Task.CurrentId}: {j}");
                        Thread.Sleep(100);
                    }
                });
                tasks_rnd.Add(t);
            }

            // way 1
            //Task.WaitAll(tasks_rnd.ToArray());

            // way 2 (without wait all)
            int finished = 0;
            while (finished != tasks_rnd.Count)
            {
                finished = 0;
                foreach(var t in tasks_rnd)
                {
                    finished += t.Status == TaskStatus.RanToCompletion ? 1 : 0;
                }
                Thread.Sleep(10);
            }

            //Barrier b = new Barrier(10);
            //b.SignalAndWait();

        }
    }
}
